<?php

namespace Drupal\birthday_wish_firework\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class BirthdayWishFireworkSettingsForm extends ConfigFormBase {

  protected function getEditableConfigNames() {
    return ['birthday_wish_firework.settings'];
  }

  public function getFormId() {
    return 'birthday_wish_firework_settings_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('birthday_wish_firework.settings');

    $form['enable_fireworks'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Fireworks'),
      '#default_value' => $config->get('enable_fireworks'),
      '#description' => $this->t('Check this box to enable fireworks on the site.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('birthday_wish_firework.settings');

    $config->set('message', $form_state->getValue('message'));
    $config->set('enable_fireworks', $form_state->getValue('enable_fireworks'));
    $config->save();

    parent::submitForm($form, $form_state);
  }
}
