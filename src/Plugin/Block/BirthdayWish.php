<?php

namespace Drupal\birthday_wish_firework\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Render\Markup;

/**
 * Provides a 'Birthday Wish Fireworks' Block.
 *
 * @Block(
 *   id = "birthday_wish_firework",
 *   subject = @Translation("Birthday Wish Block"),
 *   admin_label = @Translation("Birthday Wish based fireworks")
 * )
 */
class BirthdayWish extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $markup = Markup::create('<div class="birthday-gift">
    <div class="gift">
    <input id="click" type="checkbox">
    <label class="click" for="click"  ></label>
    <div class="wishes">Happy Birthday!</div>
         <div class="sparkles">
        <div class="spark1"></div>
        <div class="spark2"></div>
        <div class="spark3"></div>
        <div class="spark4"></div>
        <div class="spark5"></div>
        <div class="spark6"></div>
      </div>
      </div>
  </div>');
    $output = [
      '#type' => 'markup',
      '#markup' => $markup,
      '#attached' => [
        'library' => [
          'birthday_wish_firework/birthdaywish',
        ],
      ],
    ];
    return $output;
  }
}
