(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.myModuleFirework = {
    attach: function (context, settings) {
      var fireworks = drupalSettings.firework;
      var bdate = drupalSettings.bdate;
      var currentDate = new Date();
      var userBirthdate = new Date(bdate);

      var isBirthday = userBirthdate.getDate() === currentDate.getDate() && userBirthdate.getMonth() === currentDate.getMonth();

      if(fireworks === 1 && isBirthday) {
        jQuery('.path-frontpage').fireworks();
        jQuery('.path-frontpage .dialog-off-canvas-main-canvas').before(jQuery("canvas"));
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
